# Keycloak Folder Info

## Config Folder
The config floder is where all the files that are needed to configure keycloak a certian way.

This is where the standalone.xml file is located and you will make changes to this if you need to change anything to it.

## Import Folder
This is where the import file is located. So when Keycloak starts up it will import the realm-export.json file and the BNM realm and 
the roles will be there when you login.

## Scripts Folder
This folder contains the scripts to start and stop the keycloak conatiner if you just want to start keycloak by itself.

## To Start

### Windows
Make sure you command line is in the scripts folder and run the command

```
 startup.bat
```
### Mac
Make sure your terminal is in the keycloak folder and run the command

```
/scripts/startup.sh
```
If you get a zsh: permission denied run the command 

```
sudo chmod +x scripts/startup.sh
sudo chmod +x scripts/shutdown.sh
```

## To Stop
This will stop the docker keycloak conatiner
Do the same thing as you did to start it but run this command instead

### Windows
```
 shutdown.bat
```

### Mac
```
/scripts/shutdown.sh
```
## tmp folder 
This is a temp folder that is used as a volume for the keycloak conatiner.

The login folder containes all the infomation that is need to create the Login Theme for keycloak. In there is the theme.properites and that is used to allow keycloak to see the custom theme. The BNM.css file that is located in resoureces/css folder is what is used to make the theme what you want it to look like. The messages_en.properites file is used to customize what the words are like "Enter Username:" and "Enter Password:"


## Dockerfile
This docker file contains info that helps build the conatiner a certain way

### From jboss/keycloak:latest
This is what pulls the image of keycloak that is going to be used.

### User root
This is the user when it runs cmd, enterypoint, and run commands 

### Expose
This exposes the port 8080 on the conatiner and docker-compose network

### WORKDRI /

This is where you will start in the terminal on the keycloak server

### ADD /config/standalone.xml /opt/jboss/keycloak/standalone/configuration/standalone.xml

This adds the standlone.xml file from the config folder on the local computer and 
replaces the one that is one the keycloak conatiner.

This is used to stop the page from caching the web page and it allows you to use a custom Theme for keycloak. 

This is commnented out and used for testing when creating custom themes

### COPY ./tmp /opt/jboss/keycloak/themes/BNM
This copys the theme to the keycloak loaction for themes

### ENTRYPOINT [ "/opt/jboss/tools/docker-entrypoint.sh" ]
This makes the keycloak conatiner run the docker-entrypoint.sh file when it starts up.


version 1.1.0
