const express = require('express');
const Keycloak = require('keycloak-connect');
const session = require('express-session');
const path = require('path');


// create app
const app = express();
const port = 3000;

//initialize keycloak
var memoryStore = new session.MemoryStore();
var keycloak = new Keycloak( { store: memoryStore });

// initialize session
app.use(session({
  secret:'9b15ecc3-360d-4d4a-814a-51797d3ffeff',                         
  resave: false,                         
  saveUninitialized: true,                         
  store: memoryStore                       
}));

// use keycloak
app.use(keycloak.middleware());

// basic routes
app.get('/', (req, res) => {
  res.send("hello world");
})

// protected route
app.get('/exclusive', keycloak.protect(), (req, res) => {
  res.sendFile(path.join(__dirname + '/index.html'));
})

//logout
app.use(keycloak.middleware({logout: '/'}));


// start server
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
})
