## Windows version

### A little note: 
- This guide will point you to resources for enabling WSL 2 (Windows Subsystem for Linux version 2), which is needed to install and run Docker for Desktop

- To use WSL, your computer's hardware must support virtualization, and it must be enabled. The install instructions below will point you to resources for enabling virtualization in your system's BIOS.

- If your hardware does not support virtualization, or you cannot enable virtualization, then you will not be able to install Docker for Desktop. However, you can still run Docker by installing Docker Toolbox. This used to be the only way to use Docker on Windows 10 Home. It is not as effecient as Docker for Desktop, and requires you to install and interact with VirtualBox, which Docker Toolkit uses to manage a Linux virtual machine in which your containers will run. For more information about **Docker Toolbox** see [https://docs.docker.com/toolbox/toolbox_install_windows/](https://docs.docker.com/toolbox/toolbox_install_windows/) .

### Instruction

1. Enable virtualization in your BIOS.

a. Check if virtualization is enabled: [https://thegeekpage.com/how-to-check-if-virtualization-is-enabled-in-windows-10/](https://thegeekpage.com/how-to-check-if-virtualization-is-enabled-in-windows-10/)

b. Enable virtualization in BIOS: [https://2nwiki.2n.cz/pages/viewpage.action?pageId=75202968](https://2nwiki.2n.cz/pages/viewpage.action?pageId=75202968)

c. Confirm it is anabled by repeating step 1.

d. If you cannot enable virtualization in your BIOS, you will need to use **Docker Toolbox** instead — see [https://docs.docker.com/toolbox/toolbox_install_windows/](https://docs.docker.com/toolbox/toolbox_install_windows/) .

2. Update to Windows 10, version 2004 or higher.

a. Check your Windows' version: [https://support.microsoft.com/en-us/help/13443/windows-which-version-am-i-running](https://support.microsoft.com/en-us/help/13443/windows-which-version-am-i-running)

b. Use Windows Updater to install all updates available for your system.

c. Confirm your Windows' version is 2004 by repeating step 1.

d. If you still don't have version 2004 or higher Use Microsoft Update Assistant: [https://support.microsoft.com/en-us/help/3159635/windows-10-update-assistant](https://support.microsoft.com/en-us/help/3159635/windows-10-update-assistant) to manually install the latest version of Windows 10. Then confirm our Windows' version is 2004.

3. Install and enable WSL 2: [https://docs.microsoft.com/en-us/windows/wsl/install-win10](https://docs.microsoft.com/en-us/windows/wsl/install-win10).

a. In the instructions linked above, if you get an error when you run `wsl --set-default-version 2` in the previous step, update the wsl kernel: [https://aka.ms/wsl2kernel](https://aka.ms/wsl2kernel) . Then try `wsl --set-default-version 2` again.

4. Install Docker Desktop for Windows 10: [https://www.docker.com/products/docker-desktop](https://www.docker.com/products/docker-desktop)

  

## Mac version

Download and install it from [https://hub.docker.com/editions/community/docker-ce-desktop-mac](https://hub.docker.com/editions/community/docker-ce-desktop-mac).

  

Or install Homebrew [https://brew.sh/](https://brew.sh/) and then in a terminal run

  

```

brew cask install docker

```

  

Once installed, find Docker.app in your applications folder and run it. Once the docker icon in your system tray stops animating, open a terminal and try

  

```

docker --version

```

  

If this reports the docker version, you are all set.

  

> ! Note that `cask` is important above. If you leave it out, you'll install the docker client, but not the complete docker system.

## Linux version

This needed to be tested on actual linux machine, but you could search Google for how to install Docker in Linux, or the instruction can be found in here:
https://docs.docker.com/engine/install/ubuntu/
