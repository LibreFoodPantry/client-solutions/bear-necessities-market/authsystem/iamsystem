# IAMSystem

  

## Introduction

<!-- > Short blurb about what your product does.

  

One to two paragraph statement about your product and what it does.

  

![](img/header.png) -->

- The IAMSystem is the Authentication system that manages users and login, especially admins, to log into the ViewOrder system
- For more information about the IAMSystem and to learn more about it, please visit this [link](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/authsystem/logistics/-/issues/32)
- The architechture where the IAMSystem lives can be found in this [link](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/Community/-/blob/master/Architecture.MD)
- The architechture diagram of the IAMSystem can be seen in this [link](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/authsystem/logistics/-/issues/43)
  

  

## Tech stacks

  

1. [KeyCloak](https://www.keycloak.org/documentation): Keycloak is an Open Source Identity and Access management that fullfil the following functionality: User registration, User management, Role management, Authentication (login form), and Token management to provide authorization suitable for RESTful services. **Keycloak is the center of this project, and everything will be built around Keycloak**
2. [MySQL](https://dev.mysql.com/doc/refman/8.0/en/): MySQL is an open-source relational database management system. The purpose of MySQL is to store data of users.
3. [NGINX](https://hub.docker.com/_/nginx): NGINX is an open source reverse proxy server for HTTP, HTTPS, SMTP, POP3, and IMAP protocols, as well as a load balancer, HTTP cache, and a web server (origin server). NGINX will be used as this project's server
4. [Docker](https://docs.docker.com/reference/):  Docker is an open platform for develpoing, shiiping, and running applications. Docker enables you to seperate your applications from your infrastructure so you can deliver software quickly

## Installation

  

### 1. Install Docker (If you have Docker installed, skip this step)

The Docker installation guide can be found in here: [Docker-Instruction.md](Docker-Instruction.md)

  

### 2. Install Git (skip this step if you already have it on your machine)

The link can be found in here: https://git-scm.com/downloads

  

### 3. Clone IAMSystem project to your local machine

1. Look for a blue "Clone" button to right, click it, and copy the line below "Clone with HTTPS"

2. Go to your local machine, choose a suitable place

3. Open a terminal in that place, and type

```
git clone (paste that link you just copied)
```

## Development setup

<!-- Describe how to install all development dependencies and how to run an automated test-suite of some kind. Potentially do this for multiple platforms.

```sh

make install

npm test

``` -->
### Start the project
1. Go to the cloned IAMSystem folder in your local machine
2. Open **GIT** in that folder and type
```
./dev up
```
3. Wait until it shows up "Admin console listening on http://127.0.0.1:9990". The image can be seen below
![image](Images/command-line.png)
4. Open browser, go to http://localhost:8080/ . You will see an interface below
![image](Images/keycloak-welcome-page.jpg)
5. Click "Administration console", then log in using admin username and password (At the moment, **the admin username and password is admin**)
6. You are successful once you see this interface below
![image](Images/keycloak-interface.jpg)
### End the project
1. Go to the cloned IAMSystem folder in your local machine
2. Open terminal in that folder and type
```
./dev down
```

## Usage example

<!-- A few motivating and useful examples of how your product can be used. Spice this up with code blocks and potentially more screenshots.

_For more examples and usage, please refer to the [Wiki][wiki]._ -->

 
## Contributing

1. Fork it (<https://gitlab.com/yourname/yourproject/fork>)

2. Create your feature branch (`git checkout -b feature/fooBar`)

3. Commit your changes (`git commit -am 'Add some fooBar'`)

4. Push to the branch (`git push origin feature/fooBar`)

5. Create a new Pull Request

<!-- Markdown link & img dfn's -->

<!-- [npm-image]: https://img.shields.io/npm/v/datadog-metrics.svg?style=flat-square

[npm-url]: https://npmjs.org/package/datadog-metrics

[npm-downloads]: https://img.shields.io/npm/dm/datadog-metrics.svg?style=flat-square

[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square

[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics

[wiki]: https://github.com/yourname/yourproject/wiki -->

# Integration Of IAM System

## Getting the IAM Server

This process will be very similar to the intallation instructions above, but this time we will be using docker.

### Step 1: Getting the IAM System

The easiest way to get the IAM System is via docker containers. The Auth group maintains a dockerfile in the gitlab docker repository. As long as you are authenticated, or authenticate after you pull, you should be able to get the docker image. To spin up a standalone container with the IAM System running.

```sh
docker run registry.gitlab.com/librefoodpantry/client-solutions/bear-necessities-market/authsystem/iamsystem/keycloak
```

### Step 2: Adding Keycloak to docker-compose

To truly integrate keycloak, it is best to add keycloak to a docker-compose file. This will allow you to run a single command and connect everything.

```yaml
iamsystem:
  image: 
  container_name: IAMSystem
  ports: 
    - 8080:8080
  networks: 
    - [YOUR_NETWORK]
```

## Integrating the IAM System with Express

We are using [Keycloak](https://www.keycloak.org/) for authentication. 

> As a note this is still in progress, please follow [this](https://medium.com/devops-dudes/securing-node-js-express-rest-apis-with-keycloak-a4946083be51) tutorial if you want to integrate 

### Step 1: Installing Dependencies

```js
    const session = require('express-session');
    const Keycloak = require('keycloak-connect');
```
### Step 2: Integrating Keycloak into Express

```js
    const memoryStore = new session.MemoryStore();
    const keycloak = new Keycloak({ store: memoryStore });
```

### Step 3: Protecting a route





## Release History
- 0.2.1
  - Change how to start up and shutdown the IAMSystem
- 0.2.0
  - Add section initial sectino for integration of IAM System
* 0.1.1
	* Add detail instruction on how to start-up the IAMSystem project
	* Add a link that leads to the IAMSystem architecture diagram in Introduction part
* 0.1.0
	* Fix typos
	* Add more information about technologies in techstacks
	* Add some instructions in Development Setup
	* Change the order of things like Meta, Contributing, Relase history, and Development setup
* 0.0.1
	* Work in progress. Initialize README.MD

## Things that we should do in the future

## Meta

Copyright 2021, Team Auth

<a  rel="license"  href="http://creativecommons.org/licenses/by-sa/4.0/"><img  alt="Creative Commons License"  style="border-width:0"  src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"  /></a><br  />This work is licensed under a <a  rel="license"  href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
